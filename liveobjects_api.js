var request = require('request');

function renameKeys(obj, newKeys) {
    const keyValues = Object.keys(obj).map(key => {
        const newKey = newKeys[key] || key;
        return { [newKey]: obj[key] };
    });
    return Object.assign({}, ...keyValues);
}
var normaliseValue = { "temp":"Temperature","moisture":"Humidity"};

/** CO2 affluence */
var NbPersonEstimatorAffine = require("./nbPersonEstimatorEnerbeeAffine");

var nbPersonEstimatorAffineSalleReunionConference = new NbPersonEstimatorAffine({
    a: 0.017,
    b: -7.9
});

var enhanceBeacons = function (data, callback, responseHandler) {
    var requestData = {
        "sort": [{
            "timestamp": "desc"
        }],
        "query": {
            "bool": {
                "must": [{
                    "terms": {
                        "metadata.source": [config.mobileDevice]
                    }
                },
                {
                    "bool": {
                        "should": [
                        ]
                    }
                }]
            }
        }
    };
    data.forEach(function (beacon) {
        var rangetime = {
            "range": {
                "timestamp": {
                    "gt": "",
                    "lt": ""
                }
            }
        };

        var aMinuteAgo = new Date((new Date(beacon.timestamp)).getTime() - 1000 * 30);
        var aMinuteLess = new Date((new Date(beacon.timestamp)).getTime() + 1000 * 0);

        rangetime.range.timestamp.gt = aMinuteAgo.toISOString();
        rangetime.range.timestamp.lt = aMinuteLess.toISOString();
        requestData.query.bool.must[1].bool.should.push(rangetime);

    });

    /*
    requestData.query.bool.must[1].bool.should.push({
                            "range": {
                                "timestamp": {
                                    "gt": "2017-11-16T09:00:00.000Z",
                                    "lt": "2017-11-16T09:01:00.000Z"
                                }
                            }
                        });
    requestData.query.bool.must[1].bool.should.push({
                            "range": {
                                "timestamp": {
                                    "gt": "2017-11-16T08:00:00.000Z",
                                    "lt": "2017-11-16T08:01:00.000Z"
                                }
                            }
                        });	
                        */
    var options = {
        url: 'https://liveobjects.orange-business.com/api/v0/data/search/hits',
        headers: {
            'User-Agent': 'request',
            'X-API-KEY': 'e4b2e2ac38fb47bc8059f745f98fc714'
        },
        method: "POST",
        json: requestData
    };
    
    var readLastData = function (error, response, body) {
        
        if (!error && response.statusCode == 200) {
            var i = 0;
            var beacons = [];
            body = computeAffluence(body);
            //console.log(data);
            for (let el of data) {
                // console.log(el.timestamp);
                var match = false;
                var beaconTimestamp = new Date(el.timestamp);
                for (let device of body) {
                    i++;
                    //console.log(device.timestamp);
                    var deviceTimestamp = new Date(device.timestamp);
                    //console.log(Math.abs(deviceTimestamp - beaconTimestamp));
                    if (Math.abs(deviceTimestamp - beaconTimestamp) < (60 * 1000)) {
                        device.value = renameKeys(device.value,normaliseValue);
                        beacons.push(beaconObj(el, "enhanceBeacon_v1", device.value));
                        //break;
                        match = true;
                    }
                }
                if(!match){
                    beacons.push(beaconObj(el));
                }

            }
            console.log(i);
            callback(responseHandler, beacons);

        }

    }
    request(options, readLastData);
}
var beaconObj = function (device, model, value) {
    var obj = {};
    obj.streamId = device.streamId;
    obj.timestamp = device.timestamp;
    if(model){
        obj.model = model;
        obj.value = value;
    }
    obj.metadata = {};
    obj.metadata.source = device.metadata.source;
    return obj;
}
var getBeacons = function (callback, responseHandler) {
    var requestData = {
        "size": "0",
        "aggs": {
            "uniq_lest": {
                "terms": {
                    "field": "@Beacon.value.Major"
                },
                "aggs": {
                    "uniq_test": {
                        "terms": {
                            "field": "@Beacon.value.Minor"
                        },
                        "aggs": {
                            "last_value": {
                                "top_hits": {
                                    "size": 1,
                                    "sort": [
                                        {
                                            "timestamp": {
                                                "order": "desc"
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    var beaconsLastData = function (error, response, body) {
        if (!error && response.statusCode == 200) {

            var result = []
            var Major, Minor;
            body.aggregations.uniq_lest.buckets.forEach(function (element) {
                Major = element.key
                if (Major == "00") {
                    body.aggregations.uniq_lest.buckets.unshift(element);
                } else {


                    element.uniq_test.buckets.forEach(function (beacon) {
                        Minor = beacon.key
                        // console.log(element.last_value.hits.hits[0]._source);
                        result.unshift(beacon.last_value.hits.hits[0]._source)
                        result[0].metadata.source += ':' + Major + ':' + Minor
                        delete result[0]['@Beacon']

                    }, this)
                }
            }, this)
            enhanceBeacons(result, callback, responseHandler);

        }
    }

    var options = {
        //url: 'https://liveobjects.orange-business.com/api/v0/assets?size=20&connected=true',
        url: 'https://liveobjects.orange-business.com/api/v0/data/search',
        headers: {
            'User-Agent': 'request',
            'X-API-KEY': 'e4b2e2ac38fb47bc8059f745f98fc714'
        },
        method: "POST",
        json: requestData
    }
    request(options, beaconsLastData)
}

var devicesLastdata = function (devices, callback, responseHandler, affluence) {
    var requestData = {
        "size": 0,
        "aggs": {
            "tags": {
                "terms": {
                    "field": "metadata.source",
                    "size": 0,
                    "include": devices
                },
                "aggs": {
                    "last_value": {
                        "top_hits": {
                            "size": 1,
                            "sort": [{
                                "timestamp": {
                                    "order": "desc"
                                }
                            }]
                        }
                    }
                }
            }
        }
    };
    var options = {
        url: 'https://liveobjects.orange-business.com/api/v0/data/search',
        headers: {
            'User-Agent': 'request',
            'X-API-KEY': 'e4b2e2ac38fb47bc8059f745f98fc714'
        },
        method: "POST",
        json: requestData
    };
    var readLastData = function (error, response, body) {

        if (!error && response.statusCode == 200) {

            var result = [];
            //console.log(body.aggregations);
            //var info = JSON.parse(body);
            body.aggregations.tags.buckets.forEach(function (element) {
                
                // console.log(element.last_value.hits.hits[0]._source);
                result.push(element.last_value.hits.hits[0]._source);
            }, this);
            //console.log(JSON.stringify(result));

            result = computeAffluence(result);

            addMetasensor(result);
            result = explodeSensor(result);
            callback(responseHandler, result);

        }

    }
    request(options, readLastData);
}

var getSensors = function (callback, responseHandler, affluence = false) {
    //    callback(response,{"message":"ok"});
    var optionsdevicesConnected = {
        //url: 'https://liveobjects.orange-business.com/api/v0/assets?size=20&connected=true',
        url: 'https://liveobjects.orange-business.com/api/v0/assets?size=20',
        headers: {
            'User-Agent': 'request',
            'X-API-KEY': 'e4b2e2ac38fb47bc8059f745f98fc714'
        }
    };

    function devicesConnected(error, response, body) {
        var devices = [];
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            //console.log(info.totalCount);
            info.data.forEach(function (element) {
                if (element.namespace == "hubbub" || element.id == "Enerbee" || element.id == "Kinect") {
                    devices.push("urn:lo:nsid:" + element.namespace + ':' + element.id);
                }

            }, this);
            devicesLastdata(devices, callback, responseHandler, affluence);
        }
    }

    request(optionsdevicesConnected, devicesConnected);
}

var getAffluence = function (callback, responseHandler) {
    getSensors(callback, responseHandler, true);
}

var affluenceHubbub = function (noiselevel) {
    var x = noiselevel;
    switch (true) {
        case (x < 50):
            return 20;
            break;
        case (x > 49 && x < 80):
            return 40;
            break;
        case (x > 79 && x < 120):
            return 60;
            break;
        case (x > 119):
            return 80;
            break;
        default:
            return 50;
            break;
    }
}
var affluenceEnerbee = function (co2) {
    var x = nbPersonEstimatorAffineSalleReunionConference.nbPersons(co2);
    console.log("co2 " + co2 + " nbpersons " + x);
    switch (true) {
        case (x < 2):
            return 20;
            break;
        case (x > 1 && x < 5):
            return 40;
            break;
        case (x > 4 && x < 8):
            return 60;
            break;
        case (x > 8):
            return 80;
            break;
        default:
            return 50;
            break;
    }
}

var affluenceKinect = function (nbpersons) {
    var x = nbpersons;
    switch (true) {
        case (x < 2):
            return 20;
            break;
        case (x > 1 && x < 3):
            return 40;
            break;
        case (x > 2 && x < 5):
            return 60;
            break;
        case (x > 5):
            return 80;
            break;
        default:
            return 50;
            break;
    }
}

var computeAffluence = function (result) {
    var affluenceArray = [];
    result.forEach(function (element) {
        switch (element.model) {
            case "Kinect": element.value.affluence = affluenceKinect(element.value.NumberOfPersons); break;
            case "Enerbee": element.value.affluence = affluenceEnerbee(element.value.CarbonDioxine); break;
            case "hubbub_v0": element.value.affluence = affluenceHubbub(element.value.noise); break;
            default: break;

        }

    }, this);

    return result;
}
var authoriseProperty = ['NumberOfClick','noise', 'affluence', 'temp', 'moisture', 'Temperature', 'Humidity', 'CarbonDioxine', 'Pressure', 'NumberOfPersons'];
var uniqueDeviceObj = function (device, key, value) {
    var obj = {};
    obj.streamId = device.streamId;
    obj.timestamp = device.timestamp;
    obj.model = key;
    obj.value = {};
    obj.value[key] = value;
    obj.metadata = {};
    obj.metadata.source = device.metadata.source + '_' + key;
    return obj;
}
var explodeSensor = function (result) {
    var explodeResult = [];
    result.forEach(function (device) {
        //console.log(Object.keys(device.value).length);
        device.value = renameKeys(device.value,normaliseValue);
        for (const key of Object.keys(device.value)) {
            //console.log(key, device.value[key]);
            
            
            if (authoriseProperty.includes(key)) {
                explodeResult.push(uniqueDeviceObj(device, key, device.value[key]));
            }

        }

    });
    return explodeResult;
}
var addMetasensor = function (result) {
    //console.log(config);
    config.metasensor.forEach(function (element) {
        //console.log(element.source);
        var affluencesum = 0;
        var affleuncenb = 0;
        element.devices.forEach(function (devicesource) {

            //console.log(devicesource);
            result.forEach(function (device) {
                if (device.metadata.source == devicesource) {
                    affluencesum += device.value.affluence;
                    affleuncenb++;
                }
            });
        });
        if (affleuncenb > 0) {
            result.push(affluenceObj(Math.round(affluencesum / affleuncenb), element.source));
        }
    });
}
var affluenceObj = function (affluence, source) {
    var obj = {};
    obj.streamId = "Affluence";
    obj.timestamp = new Date().toISOString();
    obj.model = "affluence";
    obj.value = {};
    obj.value.affluence = affluence;
    obj.metadata = {};
    obj.metadata.source = source;
    return obj;
}

var formatMessage = function(device){
    var data = [];
    var device = JSON.parse(device);
    if(device.model == 'TiBe'){
        device.streamId = 'demoStart';
        device.metadata.source += ':' +  device.value.DeviceId;
        data.push(beaconObj(device,'demoStart',{'demoStart':true}));
        return data;
    }
    data.push(device);
    data = computeAffluence(data);
    return explodeSensor(data);

}
var getStatus = function (callback, responseHandler) {
    //    callback(response,{"message":"ok"});
    var optionsdevicesConnected = {
        //url: 'https://liveobjects.orange-business.com/api/v0/assets?size=20&connected=true',
        url: 'https://liveobjects.orange-business.com/api/v0/assets?size=20',
        headers: {
            'User-Agent': 'request',
            'X-API-KEY': 'e4b2e2ac38fb47bc8059f745f98fc714'
        }
    };

    function devicesConnected(error, response, body) {
        var devices = [];
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            //console.log(info.totalCount);
            info.data.forEach(function (element) {
                var device = {};
                device.name = element.name;
                device.connected =element.connected;
                
                    devices.push(device);
                

            }, this);
            callback(responseHandler,devices);
        }
    }

    request(optionsdevicesConnected, devicesConnected);
}
exports.getBeacons = getBeacons;
exports.getSensors = getSensors;
exports.getAffluence = getAffluence;
exports.formatMessage = formatMessage; 
exports.enhanceBeacons = enhanceBeacons; 
exports.getStatus = getStatus;