/*
 * Copyright (C) 2017 Orange
 *
 */

var liveobjects = require("./liveobjects_api.js")
var affluencefoo = require("./www/affluence.json")

// config json
var fs = require("fs");
var configfilename = "/data/config.json";
global.config = {};
fs.readFile(configfilename, function (error, data) {
    if (error) {
        console.error("read config error:  " + error.message);
    } else {
        console.log("Successful read from " + configfilename);
        try {
            config = JSON.parse(data);
        } catch (e) {
            config = {};
            return console.error(e);
        }
        console.log(config.metasensor);
    }
});

var devices = {};
/** MQTT client */
var request = require('request')
var mqtt = require('mqtt')
const urlmqtt = "mqtt://liveobjects.orange-business.com:1883"
// dominique.jezequel@orange.com
//const apiKey = "0d1bde96f14e4e7b926a10ff5f3b3b7a"

// michael_p (prod)
const apiKey = "e4b2e2ac38fb47bc8059f745f98fc714"

const routesTopic = { "router/~event/v1/data/new": 0, "router/~event/v1/data/eventprocessing/#": 0, "router/~event/v2/assets/#": 0 }
/** connect **/
console.log("MQTT::Connecting to ");
var client = mqtt.connect(urlmqtt, { username: "payload", password: apiKey, keepAlive: 30 })

/** client on connect **/
client.on("connect", function () {
    console.log("MQTT::Connected");
    client.subscribe(routesTopic)
    console.log("MQTT::Subscribed to topic:", routesTopic);
})

/** client on error **/
client.on("error", function (err) {
    console.log("MQTT::Error from client --> ", err);
})

/** client on reconnect **/
client.on("reconnect", function (err) {
    console.log("MQTT::reconnect from client --> ", err);
})

/** client on close **/
client.on("close", function (err) {
    console.log("MQTT::close from client --> ", err);
})

/** client on offline **/
client.on("offline", function (err) {
    console.log("MQTT::offline from client --> ", err);
})

client.on("message", function (topic, message) {
    message = message.toString();
    var device = [];
    if (topic.includes("/connected")) {
        //console.log("device connected " + topic);
        device = topic.split('/');
        device = "urn:lo:nsid:" + device[4] + ':' + device[5];
        if (device[5] != 'TiBe') {
            if (devices.hasOwnProperty(device)) {
                delete devices[device];
                clients.forEach(function (client) {
                    console.log("send connected true for " + device);
                    var result = { "metadata": { "source": device }, "connected": true };
                    client.sendUTF(JSON.stringify(result));


                });
            }
        }
    } else if (topic.includes("/disconnected")) {
        console.log("device disconnected " + topic);
        device = topic.split('/');
        if (device[4] == 'hubbub') {
            device = "urn:lo:nsid:" + device[4] + ':' + device[5];
            devices[device] = 'disconnected';
            clients.forEach(function (client) {
                console.log("send connected false for " + device);
                var result = { "metadata": { "source": device }, "connected": false };
                client.sendUTF(JSON.stringify(result));
            });
            var ziggyElection = function () {
                //var message  = getZiggy();
            
                clients.forEach(function (client) {
                    console.log("send ziggy new sensor");
                    var result = { "metadata": { "source": config.ziggyDevice }, "message": "providebyziggy","origin":device };
                    client.sendUTF(JSON.stringify(result));
                });
            }
            setTimeout(ziggyElection, getRandomInt(10, 15) * 1000);
        }
    } else if (topic.includes("router/~event/v1/data/new")) {
        //console.log(message);

        if (!message.includes("Beacon")) {
            clients.forEach(function (client) {
                client.sendUTF(JSON.stringify(liveobjects.formatMessage(message)));
            });
        } else {
            var arrayBeacon = [];
            arrayBeacon.push(JSON.parse(message));
            arrayBeacon[0].metadata.source +=  ':'+arrayBeacon[0].value.Major+':'+arrayBeacon[0].value.Minor;
            setTimeout(function () { liveobjects.enhanceBeacons(arrayBeacon, sendUniqueBeacon, false); }, 100);
        }
    } else if (topic.includes("router/~event/v1/data/eventprocessing/")) {
        console.log("new event");
        /*
        clients.forEach(function (client) {
            //connection.sendUTF(message.utf8Data);
            client.sendUTF(message);
        });
        */
    }
})
var sendUniqueBeacon = function (foo, data) {
    console.log("end unique beacon update");
    console.log(data);
    clients.forEach(function (client) {
        client.sendUTF(JSON.stringify(data));
    });

}


/** HTTP server */
var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    port = 8080;

var sendHttpResponse = function (response, data) {
    var headers = {};
    headers["Content-Type"] = "application/json";
    response.writeHead(200, headers);
    response.write(JSON.stringify(data));
    response.end();
}
var server = http.createServer(function (request, response) {

    // set header to handle the CORS

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With');
    response.setHeader('Access-Contrl-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    response.setHeader('Access-Control-Max-Age', '86400');
    var uri = url.parse(request.url).pathname
        , filename = path.join(process.cwd() + '/www/', uri);

    if (request.method == 'OPTIONS') {
        console.log('OPTIONS SUCCESS');
        response.end();
    } else if (request.method == 'POST') {
        console.log("POST");
        var body = '';
        request.on('data', function (data) {
            body += data;
            //console.log("Partial body: " + body);
            if (body.length > 1e6) {
                body = "";
                response.writeHead(413, { 'Content-Type': 'text/plain' }).end();
                request.connection.destroy();
            }
        });
        request.on('end', function () {
            try {
                if (uri == "/config") {
                    saveResult(body);
                } else if (uri == '/livebutton') {

                    try {
                        var livebutton = JSON.parse(body);

                        var buttonId = livebutton.button.info.substr(livebutton.button.info.length - 4); // => "09BE"
                        console.log("livebutton pushed" + buttonId);
                        var livebuttonDevice = {};
                        livebuttonDevice.streamId = "livebutton";
                        livebuttonDevice.timestamp = new Date().toISOString();
                        livebuttonDevice.model = "TiBe";
                        livebuttonDevice.value = {};
                        livebuttonDevice.value.DeviceId = buttonId;
                        livebuttonDevice.metadata = {};
                        livebuttonDevice.metadata.source = "urn:lo:nsid:livebutton:" + buttonId;
                        livebuttonDevice = JSON.stringify(livebuttonDevice);
                        clients.forEach(function (client) {
                            client.sendUTF(JSON.stringify(liveobjects.formatMessage(livebuttonDevice)));
                        });
                    } catch (e) {
                        return console.error(e);
                    }
                } else if (uri == "/connected") {
                    var device = JSON.parse(body);
                    
                    clients.forEach(function (client) {
                        console.log("send connected true for " + device.source);
                        var result = { "metadata": { "source": device.source }, "connected": device.connected };
                        client.sendUTF(JSON.stringify(result));
						
                    });
					if(!device.connected){
                        var ziggyElection = function () {
                            //var message  = getZiggy();
                        
                            clients.forEach(function (client) {
                                console.log("send ziggy new sensor");
                                var result = { "metadata": { "source": config.ziggyDevice }, "message": "providebyziggy","origin":device.source};
                                client.sendUTF(JSON.stringify(result));
                            });
                        }
						setTimeout(ziggyElection, 3000);
					}

                } else if (uri == "/newdata") {
                    if (!body.includes("Beacon")) {
                        clients.forEach(function (client) {
                            client.sendUTF(JSON.stringify(liveobjects.formatMessage(body)));
                        });
                    } else {
                        var arrayBeacon = [];
                        arrayBeacon.push(JSON.parse(body));
                        arrayBeacon[0].metadata.source +=  ':'+arrayBeacon[0].value.Major+':'+arrayBeacon[0].value.Minor;
                        setTimeout(function () { liveobjects.enhanceBeacons(arrayBeacon, sendUniqueBeacon, false); }, 100);

                    }
                } else {
                    console.log(body);
                }   


            } catch (e) {
                return console.error(e);
            }
            console.log("Body: " + body);
        });
        response.writeHead(200, { 'Content-Type': 'text/html' });
        response.end('post ok');
    }
    else {
        var uri = url.parse(request.url).pathname
            , filename = path.join(process.cwd() + '/www/', uri);

        if (uri == "/config") {
            sendHttpResponse(response, config);
            return;
        }
        if (uri == "/sensors") {
            liveobjects.getSensors(sendHttpResponse, response);
            return;
        }
        if (uri == "/beacons") {

            liveobjects.getBeacons(sendHttpResponse, response);
            return;
        }
        if (uri == "/affluence") {
            /*  var messageStatus = affluencefoo;
              var headers = {};
              headers["Content-Type"] = "application/json";
              response.writeHead(200, headers);
              response.write(JSON.stringify(messageStatus));
              response.end();
              return;
              */
            liveobjects.getAffluence(sendHttpResponse, response);
            return;
        }
        if (uri == "/status") {

            liveobjects.getStatus(sendHttpResponse, response);
            return;
        }
        /**
         * start serve static file
         */
        var contentTypesByExtension = {
            '.html': "text/html",
            '.css': "text/css",
            '.js': "text/javascript",
            '.jpg': "image/jpg",
            '.png': "image/png"
        };

        fs.exists(filename, function (exists) {
            if (!exists) {
                response.writeHead(404, { "Content-Type": "text/plain" });
                response.write("404 Not Found\n");
                response.end();
                console.log("404 " + filename);
                return;
            }


            fs.readFile(filename, "binary", function (err, file) {
                if (err) {
                    response.writeHead(500, { "Content-Type": "text/plain" });
                    response.write(err + "\n");
                    response.end();
                    return;
                }

                var headers = {};
                var contentType = contentTypesByExtension[path.extname(filename)];
                if (contentType) headers["Content-Type"] = contentType;
                response.writeHead(200, headers);
                response.write(file, "binary");
                response.end();
            });
        });

        /**
         * end serve static file
         */
    }
}).listen(parseInt(port, 10), '0.0.0.0');


/** start websocket server */
var WebSocketServer = require('websocket').server;

wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
    // put logic here to detect whether the specified origin is allowed.
    return true;
}

var clients = [];

wsServer.on('request', function (request) {
    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
        request.reject();
        console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
    }

    var connection = request.accept('echo-protocol', request.origin);
    clients.push(connection);

    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function (message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            clients.forEach(function (client) {
                //connection.sendUTF(message.utf8Data);
                client.sendUTF(message.utf8Data);
            });
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            clients.forEach(function (client) {
                //connection.sendBytes(message.binaryData);
                client.sendBytes(message.binaryData);
            });
        }
    });
    connection.on('close', function (reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
/** end websocket server */

var saveResult = function (result) {


    fs.writeFile(configfilename, result, function (error) {
        if (error) {
            console.error("write error:  " + error.message);
        } else {
            result = result.trim();

            config = JSON.parse(result);
            console.log("Successful Write to " + configfilename);
        }
    });
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}