/*
 * Copyright (C) 2017 Orange - Franck Roudet
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 */
(function() {
	var NbPersonEstimatorEnerbeeAffine = (function() {
		'use strict';
		/**
		 * @module Estimation of the number of people under controlled ventilation using a CO2 concentration sensor
		 */
		
		
		
		/**
		 * 
		 */
		function NbPersonEstimatorEnerbeeAffine(params) {
			this.updateParams(params);
		}
		
		
		/**
		 * Change all or part of params.
		 * @param {Object} params - params.
		 */
		NbPersonEstimatorEnerbeeAffine.prototype.updateParams= function(params) {
			params=params || {};
			this.a=params["a"]   || this.a || 0.0037; 	   // ax+b
			this.b=params["b"]   || this.b || -1.6; 	   
		}
		
		
		
		
		/**
		 * nbPersons .
		 * @param {int} co2 - frame from smart vent (comes from noble).
		 */
		NbPersonEstimatorEnerbeeAffine.prototype.nbPersons= function(Ci, activityType) {
			activityType=activityType || "normal";
			//console.log(bleframe)

			var n=this.a*Ci+this.b;
			return n;
			
		}
		
		return NbPersonEstimatorEnerbeeAffine;
	})();
	
	
		// Exporting block - Generic for browser and Node
	if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
		module.exports = NbPersonEstimatorEnerbeeAffine;
	} else {
		if (typeof define === 'function' && define.amd) {
			define([], function() {
				return NbPersonEstimatorEnerbeeAffine;
			});
		} else {
			window.NbPersonEstimatorEnerbeeAffine = NbPersonEstimatorEnerbeeAffine;
		}
	}

})();
		
